
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/remibonnin%2Fbase-de-donnees-patritours/HEAD)



How to launch the DDL 2022 Pandas and Jupyter Notebook:

To run various Notebook cells, use Shift+Enter, or the Run menu at top of the Notebook

Note: the Binder Notebook will need to be restarted after 10 minutes of inactivity.
